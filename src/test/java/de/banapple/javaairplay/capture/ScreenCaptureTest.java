package de.banapple.javaairplay.capture;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import de.banapple.javaairplay.capture.ImageProcessor;
import de.banapple.javaairplay.capture.ScreenCapture;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ScreenCaptureTest {

    @Autowired
    ScreenCapture screenCapture;

    @Test
    @Ignore
    public void test_captureScreen() throws IOException {

        BufferedImage image = screenCapture.captureScreen();

        image = ImageProcessor.from(image)
                .addPointer()
                .scale(1920, 1080)
                .getImage();

        ImageIO.write(image, "JPG", new File("/tmp/screenshot.jpg"));

    }
}
