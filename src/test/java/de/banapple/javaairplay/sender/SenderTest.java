package de.banapple.javaairplay.sender;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import de.banapple.javaairplay.sender.Sender;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;
import static org.hamcrest.CoreMatchers.*;

@RunWith(SpringRunner.class)
@RestClientTest(Sender.class)
public class SenderTest {

    @Autowired
    Sender sender;

    @Autowired
    private MockRestServiceServer server;

    @Test
    public void test_sendPhoto() throws IOException {

        /*
         * prepare
         */
        BufferedImage image = ImageIO.read(getClass()
                .getResourceAsStream("/screen.jpg"));

        server.expect(requestTo("http://localhost:9000/photo"))
                .andExpect(method(HttpMethod.PUT))
                .andExpect(header("Content-Length", notNullValue()))
                .andRespond(withSuccess());

        /*
         * execute
         */
        sender.sendPhoto("localhost", 9000, image);

        /*
         * verify
         */
        server.verify();
    }
}
