package de.banapple.javaairplay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class JavaAirplayApplication {

    public static void main(String[] args) {
        
        /* 
         * run the application runner and close it immediately 
         * to stop the scheduler
         */
        SpringApplication.run(JavaAirplayApplication.class, args).close();
    }
}
