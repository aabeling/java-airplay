package de.banapple.javaairplay.services;

import java.io.*;
import java.net.*;
import java.util.*;

import javax.jmdns.*;

import org.slf4j.*;
import org.springframework.stereotype.*;

@Component
public class ServiceDetector {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceDetector.class);

    private static final String SERVICE_TYPE_APPLE_TV = "_airplay._tcp.local.";

    private List<InetAddress> listNetworkAddresses() {

        List<InetAddress> validAddresses = new ArrayList<>();

        Enumeration<NetworkInterface> networkInterfaces;
        try {
            networkInterfaces = NetworkInterface.getNetworkInterfaces();
            LOG.debug("network interfaces: {}", networkInterfaces);

            while (networkInterfaces.hasMoreElements()) {

                NetworkInterface networkInterface = networkInterfaces.nextElement();
                LOG.debug("checking addresses of network interface: {}", networkInterface);

                for (InetAddress iaddress : Collections.list(networkInterface.getInetAddresses())) {
                    if (iaddress.isLoopbackAddress()) {
                        continue;
                    }
                    validAddresses.add(iaddress);
                }
            }
        } catch (IOException e) {
            throw new UncheckedIOException("failed to get network addresses", e);
        }

        LOG.debug("network addresses: {}", validAddresses);

        return validAddresses;
    }

    /**
     * Returns a list of available apple tv services.
     * 
     * @return
     * @throws IOException
     */
    public List<Service> listServices() {

        List<InetAddress> networkAddresses = listNetworkAddresses();

        try {
            List<Service> result = new LinkedList<>();
            for (InetAddress address : networkAddresses) {

                final JmDNS jmdns = JmDNS.create(address);
                ServiceInfo[] list = jmdns.list(SERVICE_TYPE_APPLE_TV, 1000);
                LOG.debug("services on address {}: {}", address, list);
                Arrays.asList(list).stream().forEach(s -> result.add(convert(s)));

                jmdns.close();
            }
            return result;
        } catch (IOException e) {
            throw new UncheckedIOException("failed to get services", e);
        }

    }

    private Service convert(ServiceInfo info) {

        Service result = new Service();
        result.setName(info.getName());
        if (info.getInet4Addresses().length > 0) {
            result.setHostname(info.getInet4Addresses()[0].getHostAddress());
        } else if (info.getInet6Addresses().length > 0) {
            result.setHostname(info.getInet6Addresses()[0].getHostAddress());
        }
        result.setPort(info.getPort());
        return result;
    }
}
