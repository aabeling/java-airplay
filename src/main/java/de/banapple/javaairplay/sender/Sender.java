package de.banapple.javaairplay.sender;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Sends images to an airplay service.
 */
@Component
public class Sender {

    private static final Logger LOG = LoggerFactory.getLogger(Sender.class);

    private RestTemplate template;

    public Sender(RestTemplateBuilder builder) {

        template = builder.build();
    }

    public void sendPhoto(
            final String host,
            final int port,
            final BufferedImage image) {

        /* prepare output stream */
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            boolean resultWrite = ImageIO.write(image, "jpg", os);
            if (!resultWrite) {
                throw new IllegalArgumentException("failed to write image");
            }
        } catch (IOException e) {
            throw new IllegalArgumentException("failed to write image", e);
        }

        byte[] data = os.toByteArray();
        LOG.debug("image size: {}", data.length);

        HttpHeaders headers = new HttpHeaders();
        headers.set("User-Agent", "java-airplay/1.0");
        headers.set("Content-Length", String.valueOf(data.length));

        final String uri = "http://" + host + ":" + port + "/photo";
        HttpEntity<byte[]> entity = new HttpEntity<>(data, headers);
        template.exchange(uri, HttpMethod.PUT, entity, String.class);

        LOG.debug("successfully sent image to {}", uri);
    }
}
