package de.banapple.javaairplay.capture;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageProcessor {

    private static final Logger LOG = LoggerFactory
        .getLogger(ImageProcessor.class);

    private static final String ICON_RESOURCE = "/computer-mouse-cursor.png";

    private static final int ICON_WIDTH = 64;

    private static final int ICON_HEIGHT = 64;

    private BufferedImage image;

    private Image cursor;

    private ImageProcessor(BufferedImage image) {

        this.image = image;
        InputStream is = this.getClass().getResourceAsStream(ICON_RESOURCE);
        try {
            cursor = ImageIO.read(is);
        } catch (IOException e) {
            throw new IllegalStateException("failed to create cursor image", e);
        }
    }

    public static ImageProcessor from(BufferedImage image) {

        return new ImageProcessor(image);
    }

    /**
     * Adds a pointer to the image at the current pointer location. This assumes
     * that the image has not been scaled yet.
     * 
     * Code taken from
     * https://stackoverflow.com/questions/2962271/how-to-capture-screen-image-with-mouse-pointer-on-it-in-java
     * 
     * @return the processor with the updated image
     */
    public ImageProcessor addPointer() {

        int x = MouseInfo.getPointerInfo().getLocation().x;
        int y = MouseInfo.getPointerInfo().getLocation().y;

        Graphics2D graphics2D = image.createGraphics();
        graphics2D.drawImage(cursor, x, y, ICON_WIDTH, ICON_HEIGHT, null);

        LOG.debug("added pointer");

        return this;
    }

    /**
     * Scales the current image to the given width and height. If the current
     * image is already smaller than the given resolution the image will not be
     * changed.
     * 
     * @param targetWidth
     * @param targetHeight
     * @return the processor with the scaled image.
     */
    public ImageProcessor scale(
        final int targetWidth,
        final int targetHeight) {

        int width = image.getWidth();
        int height = image.getHeight();
        LOG.debug("scaling image of size {}x{} to {}x{}", width, height,
            targetWidth, targetHeight);

        if (width <= targetWidth && height <= targetHeight) {
            /* image already fits */
            return this;
        }

        /*
         * calculate width and height such that it matches the aspect ratio of
         * the apple tv
         */
        int scaledHeight;
        int scaledWidth;
        float targetImageAspect = (float) targetWidth / targetHeight;
        float imageAspect = (float) width / height;
        LOG.debug("imageAspect: {}, targetImageAspect: {}", imageAspect,
            targetImageAspect);
        if (imageAspect > targetImageAspect) {
            scaledHeight = new Float(targetWidth / imageAspect).intValue();
            scaledWidth = targetWidth;
        } else {
            scaledHeight = targetHeight;
            scaledWidth = new Float(targetHeight * imageAspect).intValue();
        }
        LOG.debug("effective target resolution will be {}x{}", scaledWidth,
            scaledHeight);

        BufferedImage scaledimage = new BufferedImage(scaledWidth, scaledHeight,
            BufferedImage.TYPE_INT_RGB);
        Graphics2D g = scaledimage.createGraphics();
        g.drawImage(image, 0, 0, scaledWidth, scaledHeight, null);
        g.dispose();

        this.image = scaledimage;
        return this;
    }

    public BufferedImage getImage() {

        return image;
    }
}
