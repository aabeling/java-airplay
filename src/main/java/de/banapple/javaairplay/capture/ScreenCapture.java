package de.banapple.javaairplay.capture;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Takes a screenshot using the java.awt.Robot. See
 * https://stackoverflow.com/questions/4490454/how-to-take-a-screenshot-in-java.
 */
@Component
public class ScreenCapture {

    private static final Logger LOG = LoggerFactory.getLogger(ScreenCapture.class);

    /**
     * Takes a screenshot.
     * 
     * @return the image
     */
    public BufferedImage captureScreen() {

        LOG.debug("starting to capture screen");
        System.setProperty("java.awt.headless", "false");
        Robot robot;
        try {
            robot = new Robot();
        } catch (AWTException e) {
            throw new IllegalStateException("failed to create Robot, perhaps headless?");
        }
        Rectangle rectangle = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        BufferedImage result = robot.createScreenCapture(rectangle);
        LOG.debug("screen captured");

        return result;
    }

}
