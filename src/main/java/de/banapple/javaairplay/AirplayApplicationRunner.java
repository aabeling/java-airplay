package de.banapple.javaairplay;

import java.awt.image.BufferedImage;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import de.banapple.javaairplay.capture.ImageProcessor;
import de.banapple.javaairplay.capture.ScreenCapture;
import de.banapple.javaairplay.sender.Sender;
import de.banapple.javaairplay.services.Service;
import de.banapple.javaairplay.services.ServiceDetector;
import de.banapple.javaairplay.util.ApplicationPrinter;
import de.banapple.javaairplay.util.Scheduler;

@Component
public class AirplayApplicationRunner implements ApplicationRunner {

    private static final String OPTION_LIST_SERVICES = "list-services";

    private static final String OPTION_HOST = "host";

    private static final String OPTION_PORT = "port";

    @Autowired
    private ServiceDetector serviceDetector;

    @Autowired
    private ScreenCapture screenCapture;

    @Autowired
    ApplicationPrinter printer;

    @Autowired
    Scheduler scheduler;

    @Autowired
    Sender sender;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        if (args.containsOption(OPTION_LIST_SERVICES)) {

            listServices();

        } else if (args.containsOption(OPTION_HOST)
                && args.containsOption(OPTION_PORT)
                && !args.getOptionValues(OPTION_HOST).isEmpty()
                && !args.getOptionValues(OPTION_PORT).isEmpty()) {

            final String host = args.getOptionValues(OPTION_HOST).get(0);
            final int port = Integer.parseInt(args.getOptionValues(OPTION_PORT).get(0));

            connect(host, port);

        } else {
            printHelp();
        }

    }

    private void connect(final String host, final int port) {

        printer.println("connecting to " + host + ":" + port);
        scheduler.setRunnable(senderTask(host, port));

        waitUntilBreak();
    }

    private Runnable senderTask(final String host, final int port) {

        return () -> {
            BufferedImage screen = screenCapture.captureScreen();
            BufferedImage scaledScreen = ImageProcessor.from(screen).addPointer().scale(1920, 1080).getImage();
            sender.sendPhoto(host, port, scaledScreen);
        };

    }

    private void waitUntilBreak() {

        try {
            Thread.sleep(Long.MAX_VALUE);
        } catch (InterruptedException e) {
            /* ignore */
        }
    }

    private void listServices() {

        printer.println("services:");
        List<Service> services = serviceDetector.listServices();
        if (services.isEmpty()) {
            printer.println("no services found");
        } else {
            services.forEach(
                    s -> printer.println(s.getName()
                            + " --host=" + s.getHostname()
                            + " --port=" + s.getPort()));
        }
    }

    private void printHelp() {

        printer.println("options:");
        printer.println("--" + OPTION_LIST_SERVICES + ": lists the available services");
        printer.println("--" + OPTION_HOST + ": the host to connect to");
        printer.println("--" + OPTION_PORT + ": the port to connect to");
    }

}
