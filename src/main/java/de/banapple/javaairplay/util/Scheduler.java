package de.banapple.javaairplay.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Scheduler {

    private static final Logger LOG = LoggerFactory.getLogger(Scheduler.class);

    private Runnable runnable;

    @Scheduled(fixedDelay = 1000)
    public void runTask() {

        if (runnable != null) {
            LOG.debug("running task: {}", runnable);
            runnable.run();
        }
    }

    public void setRunnable(Runnable runnable) {

        this.runnable = runnable;
    }

}
