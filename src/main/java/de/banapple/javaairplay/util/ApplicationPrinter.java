package de.banapple.javaairplay.util;

import org.springframework.stereotype.Component;

@Component
public class ApplicationPrinter {

    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_BLUE = "\u001B[34m";
    
    private static final String PREFIX = ANSI_GREEN + "java-airplay: " + ANSI_BLUE;
    
    public void println(String line) {

        System.out.println(PREFIX + line + ANSI_RESET);
    }
}
