https://github.com/jamesdlow/open-airplay rewritten in my own style with spring-boot.

     $ ./gradlew bootrun -Pargs="--list-services"

     $ ./gradlew build
     $ java -jar build/libs/java-airplay-0.0.1-SNAPSHOT.jar --list-services